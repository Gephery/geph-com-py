// This library allows us to combine paths easily
const path = require('path');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
//const HtmlWebpackPlugin = require('html-webpack-plugin'); // Require  html-webpack-plugin plugin


module.exports = {
   entry: {
    "home": [path.resolve(__dirname, 'src/js', 'index.js'),
             path.resolve(__dirname, 'src/css', 'index.scss')],
   },
   output: {
      path: path.resolve(__dirname, '../static'),
      filename: '[name].min.js'
   },
   plugins: [
      new MiniCssExtractPlugin({
		    // Options similar to the same options in webpackOptions.output
		    // both options are optional
		    filename: "[name].min.css",
		  }),
       // No longer longer doing yarn compiled html use jinja2 in python
      // new HtmlWebpackPlugin({
      //     template: __dirname + "/src/html/index.html",
      //     inject: 'body'
      // })
   ],
   module: {
     rules: [
       {
         test: /\.(jpe?g|png|gif|svg|eot|woff|ttf|svg|woff2)$/,
         use: {
           loader: 'file-loader',
            options: {
                name: "[path][name].[ext]"
            },
         }
        },
        {
            enforce: "pre",
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "eslint-loader"
       },
       {
         test: /\.js?$/,
         exclude: /node_modles/,
         use: [{
           loader: 'babel-loader',
           query: {
             presets: ['es2015']
           }
         }]
        },
        {
          test: /\.(scss|css)$/,
            use: [
                {loader: MiniCssExtractPlugin.loader},
                {loader: "css-loader"}, // translates CSS into CommonJS
                {loader: "postcss-loader",
                 options: {
                   plugins: function() {
                     return [
                       require("precss"),
                       require("autoprefixer")
                     ]
                   }
                 }
                },
                {loader: "sass-loader"}, // compiles Sass to CSS, using Node Sass by default
            ]
        }
     ]
    },
};
