## Setup of Environment
1. Install Node.js and Yarn 
2. Go into front_end/ 
3. Run `yarn` to install dependencies. 
## Building js files 
- `yarn run dev` for dev build 
- `yarn run prod` for production build 
### Purpose of Yarn
- Can use ES6 JS. So classes and import/export are free game. 
- Uglify and many libs are easy to include. 
