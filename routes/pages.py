from flask import send_from_directory, render_template

from app import app


@app.route('/<path:path>', methods=['GET'])
def index(path):
    return send_from_directory('static', path)


@app.route('/')
def root():
    return render_template('index.html')

