import datetime

from flask import Flask, jsonify
from flask_cors import CORS
import flask_sqlalchemy as sqlalchemy
from flask_login import LoginManager

from urllib.parse import urlparse, urljoin
from flask import request, url_for
import bcrypt

# constants

# global

login_time = datetime.timedelta(hours=2)

# end constants


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc


app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///sqlalchemy-demo.db'
app.config['ENV'] = "development"

db = sqlalchemy.SQLAlchemy(app)


