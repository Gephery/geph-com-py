import os

from app import app
import subprocess
from sys import platform

# Routes
from routes.pages import *
# End Routes


def test():
    print("Doing hard core tests.")


def yarn():
    subprocess.Popen(["yarn"], cwd="./front_end/", shell=True).wait()
    subprocess.Popen(["yarn", "run", "dev"], cwd="./front_end/", shell=True).wait()


# def pip():
#     if platform == "win32":
#         subprocess.Popen(["pip", "install", "-r", "requirements.txt"])
#     else:
#         subprocess.Popen(["pip3", "install", "-r", "requirements.txt"])


def database():
    if os.path.isfile('sqlalchemy-demo.db'):
        os.remove('sqlalchemy-demo.db')
    if platform == "win32":
        subprocess.Popen(["python", "db_create.py"], shell=True).wait()
    else:
        subprocess.Popen(["python3", "db_create.py"], shell=True).wait()


def full():
    yarn()
    database()
    main()


def mod_full():
    yarn()
    main()


def prod_run():
    app.run(port=3000, host='0.0.0.0')


def main():
    app.run(debug=True, use_reloader=False)


if __name__ == '__main__':
    print("Welcome!")
    print("1. test")
    print("2. yarn")
    print("3. main")
    print("4. database")
    print("5. full(2,3,4)")
    print("6. full(2,3)")
    print("7. Production run main")
    option = int(input("jrun$ "))
    # No swtich...
    options = {
        1: test,
        2: yarn,
        3: main,
        4: database,
        5: full,
        6: mod_full,
        7: prod_run

    }
    options.get(option)()
    # main()
